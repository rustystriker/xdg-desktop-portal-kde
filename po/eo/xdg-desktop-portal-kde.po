# translation of xdg-desktop-portal-kde.pot to Esperanto
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the xdg-desktop-portal-kde package.
# Oliver Kellogg <olivermkellogg@gmail.com>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-24 00:40+0000\n"
"PO-Revision-Date: 2024-05-25 15:43+0200\n"
"Last-Translator: Oliver Kellogg <olivermkellogg@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"X-Poedit-Language: Esperanto\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "olivermkellogg@gmail.com"

#: src/accessdialog.cpp:22
#, kde-format
msgid "Request device access"
msgstr "Peti aparatan aliron"

#: src/appchooser.cpp:92
#, kde-format
msgctxt "count of files to open"
msgid "%1 files"
msgstr "%1 dosieroj"

#: src/appchooserdialog.cpp:40
#, kde-format
msgctxt "@title:window"
msgid "Choose Application"
msgstr "Elekti Aplikon"

#: src/appchooserdialog.cpp:43
#, kde-kuit-format
msgctxt "@info"
msgid "Choose an application to open <filename>%1</filename>"
msgstr "Elektu aplikaĵon por malfermi <filename>%1</filename>"

#: src/AppChooserDialog.qml:32
#, kde-format
msgctxt "@option:check %1 is description of a file type, like 'PNG image'"
msgid "Always open %1 files with the chosen app"
msgstr "Ĉiam malfermi %1 dosierojn kun la elektita programo"

#: src/AppChooserDialog.qml:67
#, kde-format
msgid "Show All Installed Applications"
msgstr "Montri Ĉiujn Instalitajn Aplikojn"

#: src/AppChooserDialog.qml:179
#, kde-format
msgid "Default app for this file type"
msgstr "Defaŭlta aplikaĵo por ĉi tiu dosiertipo"

#: src/AppChooserDialog.qml:180
#, kde-format
msgctxt "@info:whatsthis"
msgid "Last used app for this file type"
msgstr "Laste uzita apo por ĉi tiu dosiertipo"

#: src/AppChooserDialog.qml:199
#, kde-format
msgid "No matches"
msgstr "Neniuj kongruoj"

#: src/AppChooserDialog.qml:199
#, kde-kuit-format
msgctxt "@info"
msgid "No installed applications can open <filename>%1</filename>"
msgstr "Neniuj instalitaj aplikaĵoj povas malfermi <filename>%1</filename>"

#: src/AppChooserDialog.qml:203
#, kde-format
msgctxt "Find some more apps that can open this content using the Discover app"
msgid "Find More in Discover"
msgstr "Trovi Pli en Discover"

#: src/AppChooserDialog.qml:218
#, kde-kuit-format
msgctxt "@info"
msgid "Don't see the right app? Find more in <link>Discover</link>."
msgstr "Ĉu vi ne vidas la ĝustan apon? Trovu pli en <link>Discover</link>."

#: src/background.cpp:132
#, kde-format
msgid "Background Activity"
msgstr "Fona aktiveco"

#: src/background.cpp:135
#, kde-format
msgctxt "@info %1 is the name of an application"
msgid ""
"%1 wants to remain running when it has no visible windows. If you forbid "
"this, the application will quit when its last window is closed."
msgstr ""
"%1 volas resti rulanta kiam ĝi ne havas videblajn fenestrojn. Se vi "
"malpermesas tion, la aplikaĵo ĉesos kiam ĝia lasta fenestro estas fermata."

#: src/background.cpp:141 src/background.cpp:158
#, kde-format
msgctxt "@action:button Allow the app to keep running with no open windows"
msgid "Allow"
msgstr "Permesi"

#: src/background.cpp:148
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid"
msgstr "Malpermesi"

#: src/background.cpp:152
#, kde-format
msgctxt ""
"@title title of a dialog to confirm whether to allow an app to remain "
"running with no visible windows"
msgid "Background App Usage"
msgstr "Uzado de ap-oj en fono"

#: src/background.cpp:154
#, kde-format
msgctxt "%1 is the name of an application"
msgid ""
"Note that this will force %1 to quit when its last window is closed. This "
"could cause data loss if the application has any unsaved changes when it "
"happens."
msgstr ""
"Rimarku ke tio devigos %1 ĉesi kiam ĝia lasta fenestro estas fermita. Tio "
"povus kaŭzi datumperdon se la aplikaĵo havas iujn nekonservitajn ŝanĝojn "
"kiam ĝi okazas."

#: src/background.cpp:159
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid Anyway"
msgstr "Malpermesi ĉiukaze"

#: src/dynamiclauncher.cpp:76
#, kde-format
msgctxt "@title"
msgid "Add Web Application…"
msgstr "Aldoni TTT-aplikaĵon…"

#: src/dynamiclauncher.cpp:81
#, kde-format
msgctxt "@title"
msgid "Add Application…"
msgstr "Aldoni Aplikaĵon…"

#: src/DynamicLauncherDialog.qml:71
#, kde-format
msgctxt "@label name of a launcher/application"
msgid "Name"
msgstr "Nomo"

#: src/DynamicLauncherDialog.qml:90
#, kde-format
msgctxt "@action edit launcher name/icon"
msgid "Edit Info…"
msgstr "Redakti informojn…"

#: src/DynamicLauncherDialog.qml:96
#, kde-format
msgctxt "@action accept dialog and create launcher"
msgid "Accept"
msgstr "Akcepti"

#: src/DynamicLauncherDialog.qml:101
#, kde-format
msgctxt "@action"
msgid "Cancel"
msgstr "Nuligi"

#: src/filechooser.cpp:477
#, kde-format
msgid "Open"
msgstr "Malfermi"

#: src/InputCaptureDialog.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Input Capture Requested"
msgstr "Eniga Kaptado Alpetita"

#: src/InputCaptureDialog.qml:17
#, kde-format
msgctxt "The application is unknown"
msgid "An application requested to capture input events"
msgstr "Aplikaĵo alpetis kapti enigajn eventojn"

#: src/InputCaptureDialog.qml:17
#, kde-format
msgctxt "%1 is the name of the application"
msgid "%1 requested to capture input events"
msgstr "%1 alpetis kapti enigajn eventojn"

#: src/InputCaptureDialog.qml:21
#, kde-format
msgctxt "@action:button"
msgid "Allow"
msgstr "Permesi"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:101
#, kde-format
msgid "Select Folder"
msgstr "Elekti Dosierujon"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:104
#, kde-format
msgid "Open File"
msgstr "Malfermi Dosieron"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:106
#, kde-format
msgid "Save File"
msgstr "Konservi Dosieron"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:18
#, kde-format
msgid "Create New Folder"
msgstr "Krei Novan Dosierujon"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:28
#, kde-format
msgid "Create new folder in %1"
msgstr "Krei novan dosierujon en %1"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:35
#, kde-format
msgid "Folder name"
msgstr "Nomo de dosierujo"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:46
#, kde-format
msgid "OK"
msgstr "bone"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:57
#, kde-format
msgid "Cancel"
msgstr "Nuligi"

#: src/kirigami-filepicker/declarative/FilePicker.qml:101
#, kde-format
msgid "File name"
msgstr "Dosiernomo"

#: src/kirigami-filepicker/declarative/FilePicker.qml:117
#, kde-format
msgid "Select"
msgstr "Elekti"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:61
#, kde-format
msgid "Create Folder"
msgstr "Krei Dosierujon"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:71
#, kde-format
msgid "Filter Filetype"
msgstr "Filtri Dosiertipon"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:75
#, kde-format
msgid "Show Hidden Files"
msgstr "Montri Kaŝitajn dosierojn"

#: src/outputsmodel.cpp:19
#, kde-format
msgid "Full Workspace"
msgstr "Plena Laborspaco"

#: src/outputsmodel.cpp:24
#, kde-format
msgid "New Virtual Output"
msgstr "Nova Virtuala Eligo"

#: src/outputsmodel.cpp:28
#, kde-format
msgid "Rectangular Region"
msgstr "Rektangula Regiono"

#: src/outputsmodel.cpp:58
#, kde-format
msgid "Laptop screen"
msgstr "Ekrano de tekkomputilo"

#: src/region-select/RegionSelectOverlay.qml:147
#, kde-format
msgid "Start streaming:"
msgstr "Komenci streaming:"

#: src/region-select/RegionSelectOverlay.qml:151
#, kde-format
msgid "Clear selection:"
msgstr "Nuligi elekton:"

#: src/region-select/RegionSelectOverlay.qml:155
#: src/region-select/RegionSelectOverlay.qml:199
#, kde-format
msgid "Cancel:"
msgstr "Nuligi:"

#: src/region-select/RegionSelectOverlay.qml:161
#, kde-format
msgctxt "Mouse action"
msgid "Release left-click"
msgstr "Malteni maldekstran klakon"

#: src/region-select/RegionSelectOverlay.qml:165
#, kde-format
msgctxt "Mouse action"
msgid "Right-click"
msgstr "Dekstra-alklako"

#: src/region-select/RegionSelectOverlay.qml:169
#: src/region-select/RegionSelectOverlay.qml:209
#, kde-format
msgctxt "Keyboard action"
msgid "Escape"
msgstr "Eskapo"

#: src/region-select/RegionSelectOverlay.qml:195
#, kde-format
msgid "Create selection:"
msgstr "Krei elekton:"

#: src/region-select/RegionSelectOverlay.qml:205
#, kde-format
msgctxt "Mouse action"
msgid "Left-click and drag"
msgstr "Maldekstre alklaki kaj treni"

#: src/remotedesktop.cpp:175
#, kde-format
msgctxt "title of notification about input systems taken over"
msgid "Remote control session started"
msgstr "Seanco de teleregado komenciĝis"

#: src/remotedesktopdialog.cpp:23
#, kde-format
msgctxt "Title of the dialog that requests remote input privileges"
msgid "Remote control requested"
msgstr "Teleregilo petita"

#: src/remotedesktopdialog.cpp:38
#, kde-format
msgctxt "Unordered list with privileges granted to an external process"
msgid "An application requested access to:\n"
msgstr "Aplikaĵo petis aliron al:\n"

#: src/remotedesktopdialog.cpp:40
#, kde-format
msgctxt ""
"Unordered list with privileges granted to an external process, included the "
"app's name"
msgid "%1 requested access to remotely control:\n"
msgstr "%1 petis aliron por elfore stiri:\n"

#: src/remotedesktopdialog.cpp:43
#, kde-format
msgctxt "Will allow the app to see what's on the outputs, in markdown"
msgid " - Screens\n"
msgstr " - Ekranoj\n"

#: src/remotedesktopdialog.cpp:46
#, kde-format
msgctxt "Will allow the app to send input events, in markdown"
msgid " - Input devices\n"
msgstr "- Enigaparatoj\n"

#: src/RemoteDesktopDialog.qml:29 src/ScreenChooserDialog.qml:197
#, kde-format
msgid "Allow restoring on future sessions"
msgstr "Permesi restarigi en estontaj seancoj"

#: src/RemoteDesktopDialog.qml:36 src/ScreenChooserDialog.qml:205
#: src/UserInfoDialog.qml:43
#, kde-format
msgid "Share"
msgstr "Kunhavigi"

#: src/screencast.cpp:273
#, kde-format
msgctxt "Do not disturb mode is enabled because..."
msgid "Screen sharing in progress"
msgstr "Ekrandivido en progreso"

#: src/screenchooserdialog.cpp:143
#, kde-format
msgid "Screen Sharing"
msgstr "Ekrana Kunhavigo"

#: src/screenchooserdialog.cpp:173
#, kde-format
msgid "Choose what to share with the requesting application:"
msgstr "Elektu kion dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:175
#, kde-format
msgid "Choose what to share with %1:"
msgstr "Elektu kion dividi kun %1:"

#: src/screenchooserdialog.cpp:183
#, kde-format
msgid "Share this screen with the requesting application?"
msgstr "Ĉu dividi ĉi tiun ekranon kun la petanta aplikaĵo?"

#: src/screenchooserdialog.cpp:185
#, kde-format
msgid "Share this screen with %1?"
msgstr "Ĉu dividi ĉi tiun ekranon kun %1?"

#: src/screenchooserdialog.cpp:190
#, kde-format
msgid "Choose screens to share with the requesting application:"
msgstr "Elektu ekranojn por kunhavigi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:192
#, kde-format
msgid "Choose screens to share with %1:"
msgstr "Elektu ekranojn por kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:196
#, kde-format
msgid "Choose which screen to share with the requesting application:"
msgstr "Elektu la ekranon por dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:198
#, kde-format
msgid "Choose which screen to share with %1:"
msgstr "Elektu kiun ekranon kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:208
#, kde-format
msgid "Share this window with the requesting application?"
msgstr "Ĉu dividi ĉi tiun fenestron kun la petanta aplikaĵo?"

#: src/screenchooserdialog.cpp:210
#, kde-format
msgid "Share this window with %1?"
msgstr "Ĉu dividi ĉi tiun fenestron kun %1?"

#: src/screenchooserdialog.cpp:215
#, kde-format
msgid "Choose windows to share with the requesting application:"
msgstr "Elektu fenestrojn por kunhavigi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:217
#, kde-format
msgid "Choose windows to share with %1:"
msgstr "Elektu fenestrojn por kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:221
#, kde-format
msgid "Choose which window to share with the requesting application:"
msgstr "Elektu kiun fenestron por dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:223
#, kde-format
msgid "Choose which window to share with %1:"
msgstr "Elektu kiun fenestron kunhavigi kun %1:"

#: src/ScreenChooserDialog.qml:39
#, kde-format
msgid "Screens"
msgstr "Ekranoj"

#: src/ScreenChooserDialog.qml:42
#, kde-format
msgid "Windows"
msgstr "Fenestroj"

#: src/screenshotdialog.cpp:115
#, kde-format
msgid "Full Screen"
msgstr "Plenekrane"

#: src/screenshotdialog.cpp:116
#, kde-format
msgid "Current Screen"
msgstr "Nuna Ekrano"

#: src/screenshotdialog.cpp:117
#, kde-format
msgid "Active Window"
msgstr "Aktiva Fenestro"

#: src/ScreenshotDialog.qml:23
#, kde-format
msgid "Request Screenshot"
msgstr "Peti Ekrankopion"

#: src/ScreenshotDialog.qml:29
#, kde-format
msgid "Capture Mode"
msgstr "Kapta Reĝimo"

#: src/ScreenshotDialog.qml:33
#, kde-format
msgid "Area:"
msgstr "Areo:"

#: src/ScreenshotDialog.qml:38
#, kde-format
msgid "Delay:"
msgstr "Prokrasto:"

#: src/ScreenshotDialog.qml:42
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundo"
msgstr[1] "%1 sekundoj"

#: src/ScreenshotDialog.qml:47
#, kde-format
msgid "Content Options"
msgstr "Elektebloj pri Enhavo"

#: src/ScreenshotDialog.qml:51
#, kde-format
msgid "Include cursor pointer"
msgstr "Inkluzivi kursoran montrilon"

#: src/ScreenshotDialog.qml:56
#, kde-format
msgid "Include window borders"
msgstr "Inkluzivi fenestrajn randojn"

#: src/ScreenshotDialog.qml:71
#, kde-format
msgid "Save"
msgstr "Konservi"

#: src/ScreenshotDialog.qml:81
#, kde-format
msgid "Take"
msgstr "Preni"

#: src/session.cpp:174
#, kde-format
msgctxt "@action:inmenu stops screen/window sharing"
msgid "End"
msgstr "Fini"

#: src/session.cpp:277
#, kde-format
msgctxt ""
"SNI title that indicates there's a process remotely controlling the system"
msgid "Remote Desktop"
msgstr "Fora Labortablo"

#: src/session.cpp:293
#, kde-format
msgctxt "%1 number of screens, %2 the app that receives them"
msgid "Sharing contents to %2"
msgid_plural "%1 video streams to %2"
msgstr[0] "Kundividante enhavon al %2"
msgstr[1] "%1 videofluoj al %2"

#: src/session.cpp:297
#, kde-format
msgctxt ""
"SNI title that indicates there's a process seeing our windows or screens"
msgid "Screen casting"
msgstr "Ekranelsendado"

#: src/session.cpp:487
#, kde-format
msgid ", "
msgstr ", "

#: src/userinfodialog.cpp:32
#, kde-format
msgid "Share Information"
msgstr "Kunhavigi Informojn"

#: src/userinfodialog.cpp:33
#, kde-format
msgid "Share your personal information with the requesting application?"
msgstr "Ĉu kunhavigi viajn personajn informojn kun la petanta aplikaĵo?"

#: src/waylandintegration.cpp:282 src/waylandintegration.cpp:337
#, kde-format
msgid "Failed to start screencasting"
msgstr "Malsukcesis komenci ekranelsendon"

#: src/waylandintegration.cpp:649
#, kde-format
msgid "Remote desktop"
msgstr "Fora labortablo"
